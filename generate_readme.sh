#! /bin/bash

# Initialize README.md
echo -e "# My gruvbox wallpaper collection:\n" > README.md


for _file in `find . -type f`;
do
    if [[ $_file == *.jpg ]] || [[ $_file == *.png ]]; then
	# Replacing the ./ natively in bash:
	# ${main_string/search_term/replace_term}
	echo -e "## ${_file/.\//}\n" >> README.md
        echo -e "![${_file/.\//}](${_file/.\//} \"${_file/.\//}\")\n" >> README.md;
    fi
done

