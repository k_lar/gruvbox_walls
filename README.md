# My gruvbox wallpaper collection:

## pacman-ghosts.jpg

![pacman-ghosts.jpg](pacman-ghosts.jpg "pacman-ghosts.jpg")

## mate.jpg

![mate.jpg](mate.jpg "mate.jpg")

## staircase.jpg

![staircase.jpg](staircase.jpg "staircase.jpg")

## stairs.jpg

![stairs.jpg](stairs.jpg "stairs.jpg")

## not-so-gruv-city.jpg

![not-so-gruv-city.jpg](not-so-gruv-city.jpg "not-so-gruv-city.jpg")

## mate-2.jpg

![mate-2.jpg](mate-2.jpg "mate-2.jpg")

## bridge.jpg

![bridge.jpg](bridge.jpg "bridge.jpg")

## leaves.jpg

![leaves.jpg](leaves.jpg "leaves.jpg")

## mate-drink.jpg

![mate-drink.jpg](mate-drink.jpg "mate-drink.jpg")

## wave.jpg

![wave.jpg](wave.jpg "wave.jpg")

## plate-dark.jpg

![plate-dark.jpg](plate-dark.jpg "plate-dark.jpg")

## rails.jpg

![rails.jpg](rails.jpg "rails.jpg")

## bulbs.jpg

![bulbs.jpg](bulbs.jpg "bulbs.jpg")

## waterfall2.jpg

![waterfall2.jpg](waterfall2.jpg "waterfall2.jpg")

## road.jpg

![road.jpg](road.jpg "road.jpg")

## bush.jpg

![bush.jpg](bush.jpg "bush.jpg")

## leaves-wall.png

![leaves-wall.png](leaves-wall.png "leaves-wall.png")

## lights.jpg

![lights.jpg](lights.jpg "lights.jpg")

## forest-hut.jpg

![forest-hut.jpg](forest-hut.jpg "forest-hut.jpg")

## fence.jpg

![fence.jpg](fence.jpg "fence.jpg")

## leaves-2.jpg

![leaves-2.jpg](leaves-2.jpg "leaves-2.jpg")

## coffee-green.jpg

![coffee-green.jpg](coffee-green.jpg "coffee-green.jpg")

## wooden-table-coffee.jpg

![wooden-table-coffee.jpg](wooden-table-coffee.jpg "wooden-table-coffee.jpg")

## lantern.jpg

![lantern.jpg](lantern.jpg "lantern.jpg")

## cabin.jpg

![cabin.jpg](cabin.jpg "cabin.jpg")

## mate-3.jpg

![mate-3.jpg](mate-3.jpg "mate-3.jpg")

## beer.jpg

![beer.jpg](beer.jpg "beer.jpg")

## table.jpg

![table.jpg](table.jpg "table.jpg")

## hut.jpg

![hut.jpg](hut.jpg "hut.jpg")

## coffee-cup.jpg

![coffee-cup.jpg](coffee-cup.jpg "coffee-cup.jpg")

## leaves-3.jpg

![leaves-3.jpg](leaves-3.jpg "leaves-3.jpg")

## marketplace.jpg

![marketplace.jpg](marketplace.jpg "marketplace.jpg")

## comfy-room.jpg

![comfy-room.jpg](comfy-room.jpg "comfy-room.jpg")

## houseonthesideofalake.jpg

![houseonthesideofalake.jpg](houseonthesideofalake.jpg "houseonthesideofalake.jpg")

## more-coffee.jpg

![more-coffee.jpg](more-coffee.jpg "more-coffee.jpg")

## forest.jpg

![forest.jpg](forest.jpg "forest.jpg")

## leaves-hard.jpg

![leaves-hard.jpg](leaves-hard.jpg "leaves-hard.jpg")

